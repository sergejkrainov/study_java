package main.java;

public abstract class AbstrClass {
	
	int a = 9;
	
	protected int metAbstr1(){
		
		return 5;
	}
	
	abstract public int metAbstr2();
	
	AbstrClass(){
		
		System.out.println("In abstract Class!!");
		
	}

}
