package main.java;

public class Arrays {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		String strArr[] = new String[10];
		
		strArr[0] = "a";
		strArr[1] = "b";
		
		int intArr[] = {0, 1, 2};
		
		int intArrMult[][] = new int[3][];
		
		int intArrMultAnother[][] = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}};
		
		intArrMult[0] = new int[5];
		intArrMult[1] = new int[6];
		intArrMult[2] = new int[10];

		/*for (int i = 0; i < intArrMultAnother.length; i++) {
			for (int j = 0; j < intArrMultAnother[i].length; j++) {
				System.out.print(intArrMultAnother[i][j] + " ");
			}
			System.out.print("\n");
		}*/

		for ( int arr[] : intArrMultAnother) {
			for (int i : arr) {
				System.out.print(i + " ");
			}
			System.out.print("\n");
		}
	}

}
