package main.java;

import java.util.List;

public class CarsForJson {

    private String color;
    private String cost;
    private String max_speed;


    public void setColor(String color) {
        this.color = color;
    }

    public String getColor(){
        return this.color;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getCost(){
        return this.cost;
    }

    public void setMaxSpeed(String max_speed) {
        this.max_speed = max_speed;
    }

    public String getMaxSpeed(){
        return this.max_speed;
    }


}
