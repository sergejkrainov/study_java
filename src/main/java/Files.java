package main.java;

import java.io.*;
import java.nio.charset.Charset;

public class Files {

	public static void main(String[] args) throws IOException {
		
		File ff = new File(System.getProperty("user.dir") + "\\test.txt");
		
		/*FileWriter fr = new FileWriter(ff);
		String str1 = "hello,test new!\n";
		String str2 = "hello2,test2 new2!";
		fr.append(str1);
		fr.append(str2);
		fr.close();*/

		
		/*FileReader freadResult = new FileReader(ff);
		BufferedReader bfResult = new BufferedReader(freadResult);
		
		String strResult = "";
		String currLine = "";

		do {
			currLine = bfResult.readLine();
			if(currLine != null) {
				strResult += currLine + "\n";
			}
		} while(currLine != null);
		System.out.println("Text is:\n" + strResult);

		bfResult.close();*/
		
		
		try(
			FileInputStream fin = new FileInputStream(ff);
			FileOutputStream fout = new FileOutputStream(ff);
			FileOutputStream fout1 = new FileOutputStream(System.getProperty("user.dir") + "\\test1.txt");
			) {
			
		String str = "hello,test new!Another!";

		char[] chrFrom = new char["hello,test new!Another!".length()] ;

		fout.write(str.getBytes(Charset.forName("UTF-8")));

		
		int i = 0;
		int flag = 0;
		do{
			flag = fin.read();
			fout1.write((char)flag);
			/*if(flag != -1){
				chrFrom[i] = (char)flag;
			}*/
			i++;
		}while(flag != -1);
		/*System.out.println("");
		for(char c : chrFrom){
			System.out.print(c);
			fout1.write(c);
		}*/
		}catch(Exception e){
				e.printStackTrace();	
		}

	}

}
