package main.java;

 enum Fruits {
	Apple("Green", 10), Orange("Yellow"), Cherry("Red", 65);
	private String color;
	private int size;

	Fruits(String color, int size){
		this.color = color;
		this.size = size;
	}
	
	Fruits(String color){
		this.color = color;
	}
	
	public String getColor(){		
		return this.color;
	}
	
	public int getSize(){		
		return this.size;
	}
	

}
