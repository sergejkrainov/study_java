package main.java;

import com.ibm.jms.JMSBytesMessage;
import com.ibm.jms.JMSMessage;
import com.ibm.jms.JMSTextMessage;
import com.ibm.mq.jms.*;
import com.ibm.msg.client.wmq.WMQConstants;

import javax.jms.*;
import java.util.Enumeration;

public class MQLib {

	public static MQQueueConnection connectToMQ(String mqHost, String mqPort, String mqQueueManager, String mqChannel){
		MQQueueConnection connect = null;
		try {
			MQQueueConnectionFactory cf = new MQQueueConnectionFactory();
			cf.setHostName(mqHost);
			cf.setPort(Integer.parseInt(mqPort));
			//cf.setTransportType(JMSC.MQJMS_TP_CLIENT_MQ_TCPIP);
			cf.setTransportType (WMQConstants.WMQ_CM_CLIENT);
			cf.setQueueManager(mqQueueManager);
			cf.setChannel(mqChannel);
			connect = (MQQueueConnection) cf.createConnection();
		}catch(Exception e){
			e.printStackTrace();
		}
		return connect;
	}

	public static void sendMessageToMQ(String message, String queueName, String mqHost, String mqPort,
									   String mqQueueManager, String mqChannel){
		try {
			System.out.println("Send message: " + message);
			MQQueueConnection connection = connectToMQ(mqHost, mqPort, mqQueueManager, mqChannel);
			MQQueueSession session = (MQQueueSession) connection.createQueueSession(false, MQSession.AUTO_ACKNOWLEDGE);
			// MQQueue queue = new MQQueue("QM_TST1","QM_LINE_TST1");
			MQQueue queue = new MQQueue("queue:///" + queueName);
			MQQueueSender sender = (MQQueueSender) session.createSender(queue);
			BytesMessage bmessage = session.createBytesMessage();
			bmessage.writeBytes(message.getBytes());
			//bmessage.setJMSCorrelationID("414d5120514d303120202020202020205192105c4dc14e3c");
			//bmessage.setJMSMessageID("414d5120514d3031202020202020202096b4ba5c91e8483e");
			//bmessage.setStringProperty("BranchID", "4100");
//			JMSmessage.setStringProperty("JMS_IBM_Format", "MQRFH2");
//			JMSmessage.setJMSType("MQRFH2");
			// Start the connection
			connection.start();
			sender.send(bmessage);
			sender.close();
			session.close();
			connection.close();
		} catch (JMSException jmsex) {
			jmsex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static String receiveMessageFromMQ(String queueName, String mqHost, String mqPort, String mqQueueManager, String mqChannel) {

		try {
			MQQueueConnectionFactory cf = new MQQueueConnectionFactory();
			cf.setHostName(mqHost);
			cf.setPort(Integer.parseInt(mqPort));
//			cf.setTransportType(JMSC.MQJMS_TP_CLIENT_MQ_TCPIP);
			cf.setTransportType (WMQConstants.WMQ_CM_CLIENT);
			cf.setQueueManager(mqQueueManager);
			cf.setChannel(mqChannel);

			MQQueueConnection connect = (MQQueueConnection) cf.createQueueConnection();
			MQQueueSession session = (MQQueueSession) connect.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			MQQueue queue = (MQQueue) session.createQueue(queueName);

			try {
				QueueBrowser browser = null;
				browser = session.createBrowser(queue);
				// Чтение сообщений
				Enumeration<?> messages = browser.getEnumeration();

				int count = 0;
				Message message;
				// Просмотр сообщений
				System.out.println("Просмотр JMS сообщений : ");
				while (messages.hasMoreElements()) {
					message = (Message) messages.nextElement();
					System.out.println("\nСообщение " + (++count) + " :");
					System.out.println(message);
				}
				System.out.println("\nПросмотр JMS сообщений завершен\n");
			} catch (JMSException e) {
				e.printStackTrace();
			}


			MQQueueReceiver receiver = (MQQueueReceiver) session.createReceiver(queue);
			connect.start();
			JMSMessage receivedMessage = (JMSMessage) receiver.receive(10000);
			System.out.println("Received message: \n" + receivedMessage);

			byte[] arrayBt = receivedMessage.getBody(byte[].class);
			String messageBody = new String(arrayBt, "UTF-8");
			System.out.println("message  is :\n" + messageBody);

			receiver.close();
			session.close();
			connect.close();
			System.out.println("SUCCESS!!!");
			return messageBody;
		} catch (JMSException jmsex) {
			jmsex.printStackTrace();
			return "Error";
		} catch (Exception ex) {
			ex.printStackTrace();
			return "Error";
		}
	}

	public static void showAllMessagesFromMQ(String queueName, String mqHost, String mqPort, String mqQueueManager, String mqChannel) {

		try {
			MQQueueConnectionFactory cf = new MQQueueConnectionFactory();
			cf.setHostName(mqHost);
			cf.setPort(Integer.parseInt(mqPort));
			cf.setTransportType (WMQConstants.WMQ_CM_CLIENT);
			cf.setQueueManager(mqQueueManager);
			cf.setChannel(mqChannel);

			MQQueueConnection connect = (MQQueueConnection) cf.createQueueConnection();
			MQQueueSession session = (MQQueueSession) connect.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			MQQueue queue = (MQQueue) session.createQueue(queueName);

			try {
				QueueBrowser browser = null;
				browser = session.createBrowser(queue);
				// Чтение сообщений
				Enumeration<?> messages = browser.getEnumeration();

				int count = 0;
				Message message;
				// Просмотр сообщений
				System.out.println("Просмотр JMS сообщений : ");
				while (messages.hasMoreElements()) {
					message = (Message) messages.nextElement();
					byte[] arrayBt = message.getBody(byte[].class);
					String messageBody = new String(arrayBt, "UTF-8");
					System.out.println("\nСообщение " + (++count) + " :");
					System.out.println(messageBody);
				}
				System.out.println("\nПросмотр JMS сообщений завершен\n");
			} catch (JMSException e) {
				e.printStackTrace();
			}

			session.close();
			connect.close();
			System.out.println("SUCCESS!!!");

		} catch (JMSException jmsex) {
			jmsex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}