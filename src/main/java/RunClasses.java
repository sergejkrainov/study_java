package main.java;

import main.java.anotherPack.CarsAnotherpack;
import static main.java.anotherPack.CarsAnotherpack.driveStatic;

public class RunClasses {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

        driveStatic();

        CarsFirst crss = new CarsFirst();
        Cars car = new Cars("In constructor with param!");
		
		Cars carAnother1 = new Cars(100);
		
		Cars carAnother = new Cars();
		
		Cars.drive();
		
		CarsAnotherpack carsAnother = new CarsAnotherpack();

        carAnother1.color = "trtrr";
        carAnother1.drive(565);

		carsAnother.drive(20);

		CarsAnotherpack crs = new Cars();

		crs.drive1();
		
		
	}

}
