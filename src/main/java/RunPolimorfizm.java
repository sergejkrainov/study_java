package main.java;

import main.java.anotherPack.CarsAnotherpack;

public class RunPolimorfizm extends CarsAnotherpack {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		RunPolimorfizm link = new RunPolimorfizm();
		
		//link.drive(10);
		
	}
	
	@Override
	public void drive(int a) {
		
		System.out.println("I am in RunPolimorfizm");
	}
	
	public void drive2(int a) {
		
		System.out.println("I am in RunPolimorfizm");
	}
	
	RunPolimorfizm(){
		System.out.println("I am in RunPolimorfizm");
	}

}
