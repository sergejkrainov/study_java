package main.java;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class RunString {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String str = "This is rqUID=123456 string 123456  123456 for testing";

		
		//int pos = str.indexOf("123456");
		
		int pos = str.lastIndexOf("123456");
		
		System.out.println("pos = " + pos);
		
		int lengthStr = str.length();
		
		String localLog = str.substring(pos, lengthStr);		
		
		localLog = localLog.replace("123456", "Replaced");
		
		System.out.println("localLog = " + localLog);
		
		System.out.println("char is = " + localLog.charAt(0));
		
		StringBuffer strBuf = new StringBuffer(localLog);
		
		strBuf.delete(5, 10);
		
		System.out.println("Result buffer is " + strBuf.toString());

	}

}
