package main.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;

import java.util.Calendar;
import java.util.Date;

@Component
public class SpringComponent {

    @Autowired
    Calendar calendar;
    public Date getOperDay() throws Exception {

        return calendar.getTime();
    }
}
