package main.java;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import test.java.TestSpring;

import java.util.Calendar;

@Configuration
@ComponentScan(basePackageClasses = {SpringComponent.class})
public class SpringConfig {

    @Bean
    public Calendar calendar() {
        return Calendar.getInstance();
    }


}
