package main.java;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface TestAnno {
	
	String annoStr() default "annoStringDefault";
	
	int annoInt() default 25;
	String value();

}
