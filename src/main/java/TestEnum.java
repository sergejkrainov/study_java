package main.java;

public class TestEnum {

	public static void main(String[] args){
		// TODO Auto-generated method stub
		
		Fruits fruits = Fruits.Orange;
		System.out.println(Fruits.Apple.getColor());
		
		System.out.println("Color of " + fruits.toString() + " is " + fruits.getColor());
		
		System.out.println(Fruits.Apple.ordinal());
		
		
		try{
			for(Fruits fruit : Fruits.values()){
				System.out.println("Color of " + fruit.toString() + " is " + fruit.getColor());
				System.out.println("Size of " + fruit.toString() + " is " + fruit.getSize());
				System.out.println("Position is :" + fruit.ordinal());
			}
			
			fruits = Fruits.valueOf("Apple");
		}catch(Exception e2){
			
		}
		
		switch(fruits){
			case Orange:
				System.out.println("Enum is Orange");
				break;
			case Apple:
				System.out.println("Enum is Apple");
				break;
		}

	}

}
