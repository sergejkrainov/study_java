package main.java;

import main.java.InboundClasses;
import main.java.InboundClasses.InClass;
import org.junit.Test;

public class TestInboundClass {

    public static void  main(String[] args) {
        InboundClasses cl = new InboundClasses();
        //InboundClasses.InClass inCl = cl.new InClass();
        InboundClasses.InClass inCl = new InboundClasses.InClass();

        inCl.drive(50);
    }
}
