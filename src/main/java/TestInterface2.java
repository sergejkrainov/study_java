package main.java;

public interface TestInterface2 extends TestInterface{
	
	int y = 8;
	int met3();
	default int met4(){
		met5();
		return 5;
	}
	
	static int met5(){
		
		return 5;
	}

}
