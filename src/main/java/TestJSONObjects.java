package main.java;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestJSONObjects {
    public static void main(String args[]) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        /*CarsForJson bmw = new CarsForJson();
        CarsForJson audi = new CarsForJson();
        bmw.setColor("green");
        bmw.setCost("30000$");
        bmw.setMaxSpeed("220");
        audi.setColor("red");
        audi.setCost("35000$");
        audi.setMaxSpeed("240");

        List<CarsForJson> lst = new ArrayList<CarsForJson>();
        lst.add(bmw);
        lst.add(audi);

        System.out.println(mapper.writeValueAsString(lst));
        System.out.println(mapper.writeValueAsString(bmw));*/

        String json = "{\"color\":\"red\",\"cost\":\"35000$\",\"maxSpeed\":\"240\"}";
        CarsForJson cars = mapper.readValue(json, CarsForJson.class);
        System.out.println(cars);



    }
}
