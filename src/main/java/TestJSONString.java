package main.java;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

public class TestJSONString {

    public static void main (String[] args) throws ParseException {
        String jsonStr = "{" + "\n" +
                            "\"out_field\":\"value_out_field\"," + "\n" +
                            "\"cars\":[" + "\n" +
                                "{"+ "\n" +
                                    "\"color\":\"green\"," + "\n" +
                                    "\"cost\":\"30000$\"," + "\n" +
                                    "\"max_speed\":\"220\"" + "\n" +
                                "}," + "\n" +
                                "{" + "\n" +
                                    "\"color\":\"red\"," + "\n" +
                                    "\"cost\":\"35000$\"," + "\n" +
                                    "\"max_speed\":\"240\"" + "\n" +
                                "}" + "\n" +
                                 "]" + "\n" +
                           "}";
        JSONObject carJson = (JSONObject) JSONValue.parseWithException(jsonStr);
        System.out.println(carJson.toJSONString());
        JSONArray carsArr = (JSONArray)carJson.get("cars");
        System.out.println(carsArr.get(1));
        JSONObject jsObj = (JSONObject)carsArr.get(1);
        System.out.println(jsObj.get("cost"));
        jsObj.replace("cost", "40000$");
        jsObj.replace("color", "green");
        System.out.println(carJson.toJSONString());



    }
}
