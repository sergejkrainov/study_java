package main.java;

@FunctionalInterface
public interface TestLambda {
	String test(String str);
}
