package main.java;

public class TestLambdaRun /*implements TestLambda*/{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*TestLambda tst = new TestLambdaRun();
		tst.test("Hey, this is not lambda!");*/
		
		TestLambda tst = (str) -> {
			System.out.println("str = " + str);
			return str;
		};

		TestLambda tst2 = (str) -> {
			System.out.println("string param = " + str);
			return str;
		};

		tst.test("test1");
		/*tst2.test("test2");*/

		
		//test2(tst);
		
		/*test2((str) -> {
			System.out.println("str3 = " + str);
			return str;
		});*/
		
		
		
		//String param = tst.test("Hello!");
		
		//System.out.println("param = " + param);
		
		/*TestLambdaSecond tstSecond = (str) -> {
			System.out.println("Another interface!");
			System.out.println("str = " + str);
			return str;
		};
		
		String paramSecond = tstSecond.test("Hello Second!");*/
		
		LambdaTemplate obj = new LambdaTemplate();
		//TestLambda tst1 = obj::testing;
		TestLambda tst1 = LambdaTemplate::testing;
		
		String pr = tst1.test("test");
		//System.out.println(pr);
		
		
		//System.out.println(tst.test("Hey, this is lambda!"));
		
		//System.out.println(tst1.test("Hey, this is lambda!"));
		
		//test2(tst1);*/

	}
	
	/*@Override
	public String test(String str){
		System.out.println("str = " + str);
		return str;
	}*/
	
	static void test2(TestLambda obj){
		System.out.println(obj.test("Test"));
	}

}
