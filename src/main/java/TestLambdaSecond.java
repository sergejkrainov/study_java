package main.java;

@FunctionalInterface
public interface TestLambdaSecond {
	
	String test(String str);

}
