package main.java;

public class TestMQ {


    public static void main(String[] args) {

        String message = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                "<ns0:plc_acct_get xmlns:ns0=\"http://OnlineBankToEsb/OnlineBank\" xmlns:ns1=\"http://bis.ru/transport\" xmlns:ns2=\"http://rshb/transport/security\">\n" +
                "   <input xmlns:se=\"http://rshb/transport/security\" fromBranch=\"DBO2\" fromSystem=\"DBO2\" pass=\"ASYNC\" service=\"GetCardAccountByClientID_v2\" toBranch=\"PC\" toSystem=\"PC\" userDirect=\"REQUEST\">\n" +
                "       <se:MAC_info>\n" +
                "           <se:_hmac>459E32648A9D89A15FC19959E45408997E647ED431CE1DA80EA5974213065761</se:_hmac><se:_secretKeyId>DBO2_2113</se:_secretKeyId><se:algorithmName>HMAC (IETF RFC 2104) based on GOST hash</se:algorithmName><se:macDatetime>2019-06-18T11:27:05.905Z</se:macDatetime><se:signer>DBO2</se:signer><se:_signerId>DBO2</se:_signerId>\n" +
                "       </se:MAC_info>\n" +
                "       <ns1:parameters>\n" +
                "           <ns1:param>\n" +
                "               <ns1:name>DATA</ns1:name>\n" +
                "               <ns1:num>1</ns1:num>\n" +
                "               <ns1:type>DATASET</ns1:type>\n" +
                "               <ns1:value>\n" +
                "                   <?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "                   <ns0:GetCardAccountByClientIdRequest xmlns:ns0=\"http://www.rshb.ru/csm/alpha/account/get_card_account/by_client_id/201803/req\">\n" +
                "                       <ns0:DboNPClient>\n" +
                "                           <Application>\n" +
                "                               <FormListAccounts>\n" +
                "                                   <ShowOnlyAcctualAccount>true</ShowOnlyAcctualAccount>\n" +
                "                               </FormListAccounts>\n" +
                "                           </Application>\n" +
                "                           <ProcessingClientId>\n" +
                "                               <ObjectId>3473446</ObjectId>\n" +
                "                           </ProcessingClientId>\n" +
                "                       </ns0:DboNPClient>\n" +
                "                   </ns0:GetCardAccountByClientIdRequest>" +
                "               </ns1:value>" +
                "               <ns1:format>NOTCODED</ns1:format>" +
                "           </ns1:param>" +
                "       </ns1:parameters>" +
                "   </input>\n" +
                "</ns0:plc_acct_get>";

        /*MQLib.sendMessageToMQ(message, "ESB_TO_PC", "10.4.111.139",
                "1414", "QM01", "ESB.SVRCONN");*/

        /*MQLib.receiveMessageFromMQ("ESB_TO_PC", "10.4.111.139",
                "1414", "QM01", "ESB.SVRCONN");*/

        MQLib.showAllMessagesFromMQ("ESB_TO_OnlineBank", "10.4.111.139",
                "1414", "QM01", "ESB.SVRCONN");

    }
}
