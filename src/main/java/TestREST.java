package main.java;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class TestREST {

    public static void main (String[] args) {


        given().param("accountId", "40817810800020014317").when().get("https://test03.rshb.ru/ib6/rest/v11/getAccountInfo")
                .then().log().body();

        Response resp = RestAssured.get("https://test03.rshb.ru/ib6/rest/v11/getAccountInfo?accountId=40817810800020014317");
        System.out.println(resp.asString());


    }
}