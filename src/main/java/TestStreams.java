package main.java;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TestStreams {
    public static void main (String[] args){
        List<String> lst = new ArrayList<String>();
        lst.add("St1ring1");
        lst.add("St2ring2");
        lst.add("St3ringg23");

        //lst.stream().forEach(s -> System.out.println(s));

        /*List<String> lstAnother1 =  lst.stream().filter(s -> s.contains("g2")).collect(Collectors.toList());

        lstAnother1.stream().forEach(s -> System.out.println(s));*/

        /*List<String> lstAnother2 = lst.stream().map(s -> s.contains("g2")? s.replace("g", "ggg"):s.replace("g", "yyy"))
                .collect(Collectors.toList());


        lstAnother2.stream().forEach(s -> System.out.println(s));*/

        List<String> lstAnother3 = lst.stream().map(s -> s.contains("g2")? s.replace("g", "ggg"):s.replace("g", "yyy"))
                .map(TestStreams::calculateStr).collect(Collectors.toList());

        lstAnother3.stream().forEach(s -> System.out.println(s));

    }

    public static String calculateStr(String str) {

        return str.substring(0, 3);
    }

}
