package main.java;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TestingAnnotation {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		//testMeth("paramStr", 22);
		
        /*Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        String todayDateTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(date);
        System.out.println("Date =" + todayDateTime);*/
		
		TestingAnnotation t = new TestingAnnotation();
		Class<?> clt = t.getClass();

		clt.getMethod("testMeth", String.class, int.class).invoke(clt, "paramStr2", 100);
		
		Class<?> cl = Class.forName("main.java.TestingAnnotation");
        Class<?>[] paramTypes = new Class[] {String.class, int.class};
        Method m = cl.getMethod("testMeth", paramTypes);

		m.invoke(cl, "paramStr2", 100);
        
        TestAnno anno = m.getAnnotation(TestAnno.class);
        
        m.isAnnotationPresent(TestAnno.class);
        
        System.out.println("annoInt = " + anno.annoInt());
        System.out.println("annoStr = " + anno.annoStr());
        System.out.println("value = " + anno.value());
        
        //m.invoke(cl, "paramStr1", 5);
        
        //doc = (Document) m.invoke(Exeq, doc);

	}
	
	String a = "test";
	
	
	@TestAnno("Value1")
	//@TestAnno(annoStr = "ValueStr", annoInt = 10, value = "Value")
	static public void testMeth(String paramStr, int paramInt) throws Exception{
		
		System.out.println("paramStr = " + paramStr);
		System.out.println("paramInt = " + paramInt);
		
	}

	@TestAnno(annoStr = "ValueStr1", annoInt = 1034, value = "Value343")
	static public void testMethAnother(String paramStr, int paramInt) throws Exception{

		System.out.println("paramStr = " + paramStr);
		System.out.println("paramInt = " + paramInt);

	}

}
