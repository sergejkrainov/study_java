package main.java.anotherPack;

public class CarsAnotherpack {
	
	static protected int b = 10;
	public int a = 6;
	int prm = 78;
	
	protected void getTest() {
		System.out.println("getTest!");
	}
	
	
	public void drive(int a) {
		
		System.out.println("I am in CarsAnotherpack");
		System.out.println("a=" + this.a);
	}
	
	public void drive1() {
		
		System.out.println("I am in CarsAnotherpack");
	}

	static public void driveStatic() {

		System.out.println("I am in CarsAnotherpack");
	}

	public CarsAnotherpack(){
		
		System.out.println("Constructor:I am in CarsAnotherpack");
	}
}
