package main.java.test;

public class Variables {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String strParam = "param";
		strParam = "param2";
		
		byte a = 6;
		int b = a;
		a = (byte)b;
		char ch = 7;
		ch = (char)a;
		
		String strInt = "0";
		int intParam = Integer.parseInt(strInt);
		
		System.out.println(intParam);
		
		strInt = String.valueOf(b);
		System.out.println(strInt);
		
		int i = 0;
		System.out.println("b =" + b);
		b = i++;
		System.out.println("b =" + b);
		System.out.println("i =" + i);

	}

}
