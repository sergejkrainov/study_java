package test.java;
import main.java.SpringComponent;
import main.java.SpringConfig;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Calendar;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringConfig.class})
public class TestSpring {

    @Autowired
    SpringComponent comp;

    @Autowired
    Calendar calendar;

    @BeforeClass
    public static void beforeClass() {

        System.out.println("In beforeClass");

    }

    @Before
    public void before() {

        System.out.println(calendar.getTime());

        System.out.println("In before");
    }

    @Test
    public void testSpring() throws Exception {



        System.out.println(comp.getOperDay());

        Assert.assertTrue("Descr True", 5 > 4);

        Assert.assertEquals("Descr Equals", 10, 10);
    }

    @Test
    public void testSpring2() throws Exception {

        System.out.println(comp.getOperDay());
    }

    @After
    public void after() {

        System.out.println("In after");
    }

    @AfterClass
    public static void afterClass() {

        System.out.println("In afterClass");

    }

}
